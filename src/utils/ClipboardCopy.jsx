import { useState } from "react";
import Tooltip from "../components/Tooltip";
import "./ClipboardCopy.css";

function ClipboardCopy({ copyText, longText=false }) {
    const [isCopied, setIsCopied] = useState(false);
  
    // This is the function we wrote earlier
    async function copyTextToClipboard(text) {
      if ('clipboard' in navigator) {
        return await navigator.clipboard.writeText(text);
      } else {
        return document.execCommand('copy', true, text);
      }
    }
  
    // onClick handler function for the copy button
    const handleCopyClick = () => {
      // Asynchronously call copyTextToClipboard
      copyTextToClipboard(copyText)
        .then(() => {
          // If successful, update the isCopied state value
          setIsCopied(true);
          setTimeout(() => {
            setIsCopied(false);
          }, 1500);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  
    return (
      <div style={{display:"flex", flexDirection:"row", width: "fit-content"}}>
        <div style={{margin:"0 10px 0 10px"}} className="tooltip">
          <div style={longText?{width: "400px", textOverflow: "ellipsis", overflow: "hidden"}:{}}>{copyText}</div>
          <div className="tooltiptext">
            {copyText}
          </div>
        </div>
        <Tooltip onClick={handleCopyClick} child={isCopied ? 
          <div style={{fontSize: "small", height: "22px"}}>
            Copied!
          </div>
           : 
          <img src={require('../assets/copy.png')} height="18px" alt="clipboardIcon"style={{cursor: "pointer"}}/>}>
          {!isCopied ? "copy":""}
        </Tooltip>
      </div>
    );
  }

  export default ClipboardCopy;