import "./Tooltip.css";

const Tooltip = ({child, children, onClick}) => {
    return (
        <div onClick={onClick} className="tooltip">
            {child}
            <div className="tooltiptext">
                {children}
            </div>
        </div>
    );
}

export default Tooltip;