import "./App.scss";
import { Route, Routes } from "react-router-dom";
import Login from "./pages/Login/Login";
import Dashboard from "./pages/Dashboard/Dashboard";
import LandingComplete from "./pages/Landing/LandingComplete";
import RegisterForm from "./pages/Register/Register";
import Settings from "./pages/Settings/Settings";
import SelectFromSite from "./pages/Create/SelectFromSite";
import './i18n';
import { Suspense } from 'react';
import { useTranslation} from 'react-i18next';

function App() {
  const { t, i18n } = useTranslation();
  return (
    <div className="App">
      <Suspense fallback="...loading">
        <>{t('main.header')}</>
        <Routes>
          <Route path="/dashboard" element={ <Dashboard title="Dashboard"/> } />
          <Route path="/login" element={ <Login title="Login"/> } />
          <Route path="/register" element={ <RegisterForm title="Register"/> }/>
          <Route path="/" element={ <LandingComplete/>} />
          <Route path="/settings" element={<Settings/>}/>
          <Route path="/select-site-links" element={<SelectFromSite/>} />
        </Routes>
      </Suspense>
    </div>
  );
}

export default App;
