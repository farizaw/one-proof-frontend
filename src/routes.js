import React from "react";
import { Redirect, Switch, Route, Router } from "react-router-dom";
import RouteGuard from "./components/RouteGuard"
 
//history
import { history } from './helpers/history';
 
//pages
//import HomePage from "./pages/HomePage"
//import LoginPage from "./pages/Login"
import Login from "./pages/Login/Login";
import Dashboard from "./pages/Dashboard/Dashboard";
 
function Routes() {
   return (
       <Router history={history}>
           <Switch>
               <RouteGuard
                   exact
                   path="/"
                   component={Dashboard}
               />
               <Route
                   path="/login"
                   component={Login}
               />
               <Redirect to="/" />
           </Switch>
       </Router>
   );
}
 
export default Routes