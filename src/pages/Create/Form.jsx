// Form.js
import { Button, Input } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const isValidUrl = urlString=> {
  var urlPattern = new RegExp('^(https?:\\/\\/)?'+ // validate protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // validate domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // validate OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // validate port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // validate query string
  '(\\#[-a-z\\d_]*)?$','i'); // validate fragment locator
return !!urlPattern.test(urlString);
}

function Form() {
  const [url, setUrl] = useState('');
  const [disableSubmit, setDisableSubmit] = useState(true);
  const navigate = useNavigate();


  useEffect(()=>{
    setDisableSubmit(!isValidUrl(url));
  }, [url]);

  const handleSubmit = (e) => {
    e.preventDefault();
    // Redirect to the injected site with the provided URL
    navigate(`/select-site-links?url=${encodeURIComponent(url)}`);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Input
          type="text"
          value={url}
          onChange={(e) => setUrl(e.target.value)}
          placeholder="Enter URL"/>
        <Button
        disabled={disableSubmit} type="submit"
        variant="contained"
        style={{textTransform: "none"}}>
          Go and Select PDFs
        </Button>
      </form>
    </div>
  );
}

export default Form;
