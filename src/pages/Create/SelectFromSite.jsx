// SelectFromSite.js
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Button } from '@mui/material';
import './SelectFromSite.scss';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';

/*TODOS
TODO: Popup warning when back
TODO: Popup error: no selected
TODO: Popup success: succesfully sent
TODO: header stays during scrolling (sticky navbar)
TODO: favicon.ico One Proof
TODO: title router
TODO: APLNG: CORS Issue
TODO: PrinceXML: CSS issue
TODO: 
*/


function SelectFromSite() {
  const location = useLocation();
  const navigate = useNavigate();
  const [url, setUrl] = useState('');
  const [htmlContent, setHtmlContent] = useState('');
  const [verifyResponse, setVerifyResponse] = useState('');

  useEffect(() => {
    // Extract the URL parameter from the query string
    const queryParams = new URLSearchParams(location.search);
    const urlParam = queryParams.get('url');
    setUrl(urlParam);

    // // Fetch HTML content from the provided URL
    // const fetchHtmlContent = async () => {
    //   try {
    //     const response = await axios.get(urlParam);
    //     setHtmlContent(response.data);
    //   } catch (error) {
    //     console.error('Error fetching HTML content:', error);
    //   }
    // Fetch HTML content from the proxy server
    const fetchHtmlContent = async () => {
      try {
        const response = await axios.post('http://localhost:8000/api/fetch-html/', {
          url: urlParam
        }, {
          headers: {
            'Authorization': "JWT " + sessionStorage.getItem('access_token'),
          }
        });

        setHtmlContent(response.data.html);
      } catch (error) {
        console.error('Error fetching HTML content:', error);
      }
      //console.log("htmlcontent:", htmlContent);
    };

    fetchHtmlContent();
  }, [location.search]);

  //useEffect(()=>{console.log("changes happen")},[htmlContent]);

  // TODO: Implement logic to inject checkboxes and download button into the site

  const normalizeLinks = (html) => {
    const parser = new DOMParser();
    const doc = parser.parseFromString(html, 'text/html');
  }

  const injectComponents = (html) => {
    const parser = new DOMParser();
    const doc = parser.parseFromString(html, 'text/html');

    // Example: Inject checkboxes next to all <a> tags that link to PDF files
    const pdfLinks = doc.querySelectorAll('a[href$=".pdf"]');
    pdfLinks.forEach((link) => {
      const checkbox = doc.createElement('input');
      checkbox.type = 'checkbox';
      checkbox.value = link.href;
      checkbox.onchange = ()=>{console.log("changes happen")};
      link.parentNode.insertBefore(checkbox, link.nextSibling);
    });

    return doc.documentElement.outerHTML;
  };


  const handleDownload = () => {
    console.log("handleDownload");
    // Example: Retrieve checked PDF links and initiate download
    const checkedCheckboxes = document.querySelectorAll('input[type="checkbox"]:checked');
    const pdfLinks = Array.from(checkedCheckboxes).map((checkbox) => checkbox.value);
    console.log("PDFLINKS: ", pdfLinks.length);

    // Example: Trigger downloads (you may need to adjust this logic based on your needs)
    // pdfLinks.forEach((pdfLink) => {
    //   console.log("pdfLink: ", pdfLink);
    //   const link = document.createElement('a');
    //   link.href = pdfLink;
    //   link.target = '_blank';
    //   link.download = pdfLink.substring(pdfLink.lastIndexOf('/') + 1);
    //   link.click();
    // });

    const verifyPdfs = async () => {
      try {
        const response = await axios.post('http://localhost:8000/api/verify/', {
          pdfLinks: pdfLinks,
        }, {
          headers: {
            'Authorization': "JWT " + sessionStorage.getItem('access_token'),
          }
        });
        setVerifyResponse(response);
        console.log("verifyResponse: ", verifyResponse);
      } catch (error) {
        console.error('Error fetching HTML content:', error);
      }
    };
    verifyPdfs();
    navigate(-1);
  };

  return (
    <div>
      <div className='top-bar'>
        <ArrowBackIcon onClick={()=>{navigate(-1)}}/>
        <label>Site:</label>
        <input type="text" value={url} disabled style={{width: "500px"}}/>
        {/* Render the injected site with checkboxes and download button */}
        <Button
        onClick={handleDownload}
        variant="contained"
        style={{textTransform: "none"}}>
          Verify Selected Links
        </Button>
      </div>
      
      <div dangerouslySetInnerHTML={{ __html: injectComponents(htmlContent) }} />
      

    </div>
  );
}

export default SelectFromSite;
