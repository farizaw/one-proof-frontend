import { Link } from 'react-router-dom';
import '../CertificateMenu.scss';
import { Button, CircularProgress, Input } from '@mui/material';
import { useState } from 'react';
import Form from './Form';

const CreateCertificate = () => {
    const [url, setUrl] = useState("");
    const [frameLoad, setFrameLoad] = useState(false);
    const [loading, setLoading] = useState(true);
    
    const hideSpinner = () => {
        setLoading(false);
    };

    const handleInputChange = e => {
        setUrl(e.target.value);
    }

    const handleSubmitUrl = () => {
        setFrameLoad(true);
    }

    return (
    <div className="Content">
    <div className="Content-Header">
        <div className="Content-Header-Title">Create a Proof Certificate</div>
            <Link to="/" className="Content-Header-Link">View Sample Proof Certificate</Link>
        </div>
        <div className="Content-Input-Group">
            <Form/>
        </div>
        
    </div>
    );
}

export default CreateCertificate;