// HtmlRenderer.js
import React, { useState } from 'react';
import axios from 'axios';

function HtmlRenderer() {
  const [url, setUrl] = useState('');
  const [htmlContent, setHtmlContent] = useState('');

  const handleUrlChange = (e) => {
    setUrl(e.target.value);
  };

  const handleRender = async () => {
    if (url.trim() !== '') {
      try {
        // Use Axios to fetch HTML content from the provided URL
        const response = await axios.get(url);
        const html = response.data;

        // Open a new tab and render the HTML content
        const newTab = window.open();
        newTab.document.write(html);

        // Save the HTML content in the component state for reference
        setHtmlContent(html);
      } catch (error) {
        console.error('Error fetching or rendering HTML:', error);
      }
    }
  };

  return (
    <div>
      <label>
        Enter URL:
        <input type="text" value={url} onChange={handleUrlChange} />
      </label>
      <button onClick={handleRender}>Render HTML in New Tab</button>

      {htmlContent && (
        <div>
          <h2>Rendered HTML Content:</h2>
          <pre>{htmlContent}</pre>
        </div>
      )}
    </div>
  );
}

export default HtmlRenderer;
