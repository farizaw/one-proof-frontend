import './Login.scss';
import logo from '../../assets/oneproof.png';
import { Link } from 'react-router-dom';
import { Button } from '@mui/material';
import { useEffect, useState } from 'react';
import axios from 'axios';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';

const apiUrl = process.env.API_URL || "http://oneproof-django-env.eba-k9k2ac2e.ap-southeast-2.elasticbeanstalk.com";
const loginEndpoint = `${apiUrl}/api/token/obtain/`;

const parseJwt = token => {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

const Login = () => {
    const [errorLogin, setErrorLogin] = useState(false);
    const [passwordVisible, setPasswordVisible] = useState(false);

    useEffect(()=>{
        document.title="Login";
    },[]);
    const [credentials, setCredentials] = useState({
        username: '',
        password: ''
    })


    // onClick handler function for the copy button
    const handlePasswordVisibilityClick = () => {
        setPasswordVisible(true);
        setTimeout(() => {
            setPasswordVisible(false);
        }, 1500);
      }

    const handleSubmit = async () => {
        console.log("credentials: ", credentials);
      
        await axios.post(loginEndpoint, credentials)
          .then(response => {
            //get token from response
            const token  =  response.data;
            //console.log('response: ', response);
      
            //set JWT token to local
            sessionStorage.setItem("username", parseJwt(token.access).username);
            sessionStorage.setItem("access_token", token.access);
            sessionStorage.setItem("refresh_token", token.refresh);
      
            //set token to axios common header
            //setAuthToken(token);
      
            //redirect user to home page
            window.location.href = '/dashboard'
          })
          .catch(err => {
            setErrorLogin(true);
            console.log("err: ", err.message)
        });
    }

    return(
        <div className="App-page">
            <div className='Login-Card'>
                <div className='Login-Title'>Login to your OneProof Account</div>
                <div className='Login-Logo'>
                    <img src={logo} alt="logo" width={"100px"}/>
                </div>
                <div className='Login-Input-Group'>
                    <label>Username</label>
                    <div className="Login-Input-Form">
                        <input className='Login-Input' type='text' style={{width: "96%"}} placeholder='username' value={credentials.username} onChange={e=>{setCredentials({...credentials, username: e.target.value})}}></input></div>
                </div>
                <div className='Login-Input-Group'>
                    <label>Password</label>
                    <div className="Login-Input-Form">
                        <input className='Login-Input' type={passwordVisible ? 'text': 'password'} placeholder='password' value={credentials.password} onChange={e=>{setCredentials({...credentials, password: e.target.value})}}></input>
                        <div onClick={handlePasswordVisibilityClick} style={{padding: "2px 4px 0 4px", border: "lightgray solid 1px"}}>{passwordVisible ? <VisibilityOffIcon  fontSize="x-small"/>: <VisibilityIcon  fontSize="x-small"/>}</div>
                    </div>
                </div>
                {errorLogin?<div className='Login-Error'><strong>Error: </strong>incorrect credentials provided</div>:<></>}
                <div className='Login-Forgot-Password'><a href="/">forgot your password?</a></div>
                <div className='login-empty-separator'/>
                <Button variant="contained" style={{textTransform:"none"}}onClick={handleSubmit}>Login</Button>
                <div className='Login-Subtitle'>{"Don't have an account yet? Register "}
                    <Link to="/register">{"here"}</Link>
                </div>
            </div>
        </div>
        );
}

export default Login;