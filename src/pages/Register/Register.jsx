import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import "./Register.scss";
import { Link } from "react-router-dom";
import { Button } from "@mui/material";
import axios from "axios";

const RegisterForm = () => {
  const { register, handleSubmit, formState: { errors }, getValues } = useForm();
  const [registrationSuccess, setRegistrationSuccess] = useState(false);
  const handleRegistration = async (data) => {
        console.log("data: ", data);
        await axios.post("http://127.0.0.1:8000/api/user/create/", data)
          .then(response => {
            console.log("response", response);
            setRegistrationSuccess(true);
          })
          .catch(err => {
            console.log("err: ", err.message)
        });
    console.log("data", data)
  };
  
  const handleError = (errors) => {console.log("error: ", errors)};

  useEffect(()=>{
    document.title="Register";
  },[]);

  const registerOptions = {
    first_name: { required: "First Name is required" },
    last_name: { required: "Last Name is required" },
    username: { required: "Username is required" },
    email: { required: "Email is required" },
    password: {
      required: "Password is required",
      minLength: {
        value: 8,
        message: "Password must have at least 8 characters"
      }
    },
    confirmPassword: {
      required: "Password is required",
      minLength: {
        value: 8,
        message: "Password must have at least 8 characters"
      },
      validate: (value) => {
        const { password } = getValues();
        return password === value || "Passwords should match!";
      },
    },
  };

  return (
    <div className="register-page">
      <div className="register-card">
        <div className="register-title">Create your One Proof Account</div>
      {registrationSuccess
      ? <div style={{height: "300px", fontSize:"medium"}}>successfully registered. go to <Link to="/login">{"login"}</Link> page</div>
      : <form onSubmit={handleSubmit(handleRegistration, handleError)}>
        <div className="register-full-name-input-group">
          <div className="register-input-group">
            <label>First Name</label>
            <input className="register-input-short" name="name" type="text" {...register('first_name', registerOptions.first_name) }/>
            <small className="register-text-danger">
              {errors?.first_name && errors.first_name.message}
            </small>
          </div>

          <div className="register-input-group">
            <label>Last Name</label>
            <input className="register-input-short" name="name" type="text" {...register('last_name', registerOptions.last_name) }/>
            <small className="register-text-danger">
              {errors?.last_name && errors.last_name.message}
            </small>
          </div>
        </div>

        <div className="register-input-group">
          <label>Username</label>
          <input className="register-input" name="name" type="text" {...register('username', registerOptions.username) }/>
          <small className="register-text-danger">
              {errors?.username && errors.username.message}
          </small>
        </div>

        <div className="register-input-group">
          <label>Email</label>
          <input className="register-input"
            type="email"
            name="email"
            {...register('email', registerOptions.email)}
          />
          <small className="register-text-danger">
            {errors?.email && errors.email.message}
          </small>
        </div>

        <div className="register-input-group">
          <label>Password</label>
          <input className="register-input"
            type="password"
            name="password"
            {...register('password', registerOptions.password)}
          />
          <small className="register-text-danger">
            {errors?.password && errors.password.message}
          </small>
        </div>

        <div className="register-input-group">
          <label>Confirm Password</label>
          <input className="register-input"
            type="password"
            name="password"
            {...register('confirmPassword', registerOptions.confirmPassword)}
          />
          <small className="register-text-danger">
            {errors?.confirmPassword && errors.confirmPassword.message}
          </small>
        </div>
        <Button type="submit" variant="contained" style={{textTransform:"none"}}>Register</Button>
        {/* <button className="register-submit-button">Register</button> */}
        <div className='Login-Subtitle'>{"Have an account already? Login "}
                    <Link to="/login">{"here"}</Link>
        </div>
      </form>}
      </div>
    </div>
  );
};
export default RegisterForm;