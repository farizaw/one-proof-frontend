import { useEffect, useState } from 'react';
import '../CertificateMenu.scss';
import data_ from '../../data/dummydata.json';
import ClipboardCopy from '../../utils/ClipboardCopy';
import { Button, Select } from '@mui/material';

const RetrieveCertificate = () => {
    const pageSizes = [10, 25, 50, 100];
    const [pageSize, setPageSize] = useState(pageSizes[0]);
    const [currentPage, setCurrentPage] = useState(0); // page 0 = first page
    const data = data_["data"];

    useEffect(() => {
        setCurrentPage(0);
    },[pageSize]);

    return (
    <div className='Content'>
    <div className="Content-Header">
        <div className="Content-Header-Title">Retrieve a Proof Certificate</div>
            <div className="Content-Header-Subtitle">Please select a previous <b>Proof Certificate</b></div>
        </div>

        <div>
            <div className='pagination-status'>
                <div className='pagination-status-page-size'>
                    <label for="pageSizeSelect">Showing </label>
                    <select name="pageSizeSelect"
                    id="pageSizeSelect"
                    onChange={e => setPageSize(e.target.value)}
                    style={{"backgroundColor": "white", border: "none", fontSize: "medium"}}>
                        {pageSizes.map(size => <option value={size}>{size}</option>)}
                    </select>
                    <label> items each page</label>
                </div>
                <div>
                    Page {currentPage + 1} of {Math.floor((data.length-1) / pageSize) + 1}
                </div>
            </div>

        <table>
                <tr style={{backgroundColor: "LightSteelBlue"}}>
                    <th>Created on</th>
                    <th>Proof Certificate Details</th>
                    <th >Actions</th>
                </tr>
                {data.slice(currentPage * pageSize, (currentPage + 1) * pageSize).map((val, key) => {
                    return (
                        <tr key={key} style={key%2 === 0?{backgroundColor: "AliceBlue"}:{}}>
                            <td style={{display:"flex", flexDirection: "row"}}>
                                <div style={{verticalAlign:"top", fontSize: "small"}}>
                                    <div style={{width: "180px"}}>{val.datetime}</div>
                                </div>
                            </td>
                            <td style={{fontSize: "small", textAlign: "left", padding: "0 10px 30px 10px", width: "700px"}}>
                                <div style={{fontSize: "large", display:"flex"}}><b>Certificate Number: </b><ClipboardCopy copyText={val.certificate_number}/>
                                </div>

                                <div><b>Reference/Description: </b>{val.description}
                                </div>

                                <div><b>Source url: </b><a href={val.source_url}>{val.source_url}</a>
                                </div>

                                <div><b>File name: </b>{val.file_name}</div>
                                <div><b>Accessed: </b>{val.datetime}
                                </div>

                                <div style={{display: "flex", flexDirection: "row"}}><b>File hash (SHA256): </b><ClipboardCopy longText copyText={val.filehash}/></div>
                            </td>
                            <td style={{display:"flex", flexDirection: "row", fontSize: "small"}}>
                                <div style={{paddingLeft:"8px", paddingRight:"8px"}}>Download</div>
                                <div>Verify</div>
                                <div style={{paddingLeft:"8px", paddingRight:"8px"}}>Delete</div>
                            </td>
                        </tr>
                    )
                })}
        </table>
        </div>
        <div>
            <Button style={{textTransform: "none"}}className='page-navigation'
            onClick={()=>{setCurrentPage(0)}}
            disabled={currentPage === 0}>
                First
            </Button>
            
            <Button style={{textTransform: "none"}}className='page-navigation'
            onClick={()=>{setCurrentPage(currentPage - 1)}}
            disabled={currentPage <= 0}>
                Prev
            </Button>
            
            <Button style={{textTransform: "none"}}className='page-navigation'
            onClick={()=>{setCurrentPage(currentPage + 1)}}
            disabled={currentPage >= Math.floor((data.length-1) / pageSize)}>
                Next
            </Button>
            
            <Button style={{textTransform: "none"}}className='page-navigation'
            onClick={()=>{setCurrentPage(Math.floor((data.length-1) / pageSize))}}
            disabled={currentPage === Math.floor((data.length-1) / pageSize)}>
                Last
            </Button>

        </div>
        
    </div>
    );
}

export default RetrieveCertificate;