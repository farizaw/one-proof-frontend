import './Dashboard.css';
import logo from '../../assets/oneproof1.png'
import { useEffect, useState } from 'react';
//import Login from '../Login/Login';
import CreateCertificate from '../Create/CreateCertificate';
import RetrieveCertificate from '../Retrieve/RetrieveCertificate';
import VerifyCertificate from '../Verify/VerifyCertificate';
import MenuItems from '../../components/MenuItems';
import { Button } from '@mui/material';

const logout = () => {
    sessionStorage.removeItem("username");
    sessionStorage.removeItem('access_token');
    sessionStorage.removeItem('refresh_token');
    window.location.href = '/';
}

const goToSettings = () => {
    window.location.href = '/settings';
}

const menuItemsData = [
    {
      title: 'My Account',
      url: '/about',
      submenu: [
          {
            title: 'johnsmith',
            url: 'name',
            func_: () => {},
          },
          {
            title: 'settings',
            url: 'settings',
            func_: () => {},
          },
          {
            title: 'help',
            url: 'help',
            func_: () => {},
          },
          {
            title: 'logout',
            url: 'logout',
            func_: () => {
                logout();
            },
          },
      ],
    },
  ];

const Dashboard = () => {

    const [activeTab, setActiveTab] = useState('CREATE');
    const [name, setName] = useState('');

    useEffect(()=>{
        setName(sessionStorage.getItem("username"));
    }, [])

    const content = {
        'CREATE' : <CreateCertificate/>,
        'RETRIEVE' : <RetrieveCertificate/>,
        'VERIFY' : <VerifyCertificate/>,
    }
    return(
    <div>
        <div className="Header">
            <div className="Header-Logo">
                <img src={logo} className="Header-Logo-Logo" alt="logo" />
            </div>
            <div className="Header-Menu-and-Title">
                <div className="Header-Title">
                    <span className='span-title'>
                        Welcome back
                    </span>
                </div>
                <div className="Header-Menus">
                    <div className="Header-Menu-Tabs">
                        <div
                        className={'Menu-Tab'+ (activeTab === 'CREATE'  ? '-active':'')}
                        onClick={()=>{setActiveTab('CREATE')}}>
                            <span className='span-menu'>
                                Create
                            </span>
                        </div>
                        <div
                        className={'Menu-Tab'+ (activeTab === 'RETRIEVE' ?'-active':'')}
                        onClick={()=>{setActiveTab('RETRIEVE')}}>
                            <span className='span-menu'>
                                Retrieve
                            </span>
                        </div>
                        <div
                        className={'Menu-Tab'+ (activeTab === 'VERIFY'  ? '-active':'')}
                        onClick={()=>{setActiveTab('VERIFY')}}>
                            <span className='span-menu'>
                                Verify
                            </span>
                        </div>
                    </div>
                    <div className="Header-Menu-Separator">Separator</div>
                    <div className='Header-Right-Menus'>
                        <div className='Header-Right-Menu-Name'>
                            <strong className='span-right-menu'>{name}</strong>
                        </div>
                        <div onClick={goToSettings} className='Header-Right-Menu'>
                            <span className='span-right-menu'>Settings</span>
                        </div>
                        <div className='Header-Right-Menu'>
                            <span className='span-right-menu'>Help</span>
                        </div>
                        <div onClick={logout} className='Header-Right-Menu'>
                            <span className='span-right-menu'>Logout</span>
                        </div>
                        <div>
      
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="Body">
            {content[activeTab]}
        </div>
    </div>);
    
}

export default Dashboard;