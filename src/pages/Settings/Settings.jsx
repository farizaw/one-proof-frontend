import { Button, Input } from "@mui/material";
import React, { useState } from "react";
import TimezoneSelect from "react-timezone-select";
import logo from '../../assets/oneproof1.png';
import EditIcon from '@mui/icons-material/Edit';
import './Settings.css';
import axios from "axios";

const logout = () => {
  sessionStorage.removeItem("username");
  sessionStorage.removeItem('access_token');
  sessionStorage.removeItem('refresh_token');
  window.location.href = '/';
}

const goToDashboard = () => {
  window.location.href = '/dashboard';
}

const Settings = () => {
    const [selectedTimezone, setSelectedTimezone] = useState({});
    const [hasChanged, setHasChanged] = useState(false);
    const [field, setField] = useState({first_name: "John", last_name: "Smith", username: "john.smith1", email: "johnsmith@mail.com"});
    const [changePassword, setChangePassword] = useState(false);
    const [oldPassword, setOldPassword] = useState('');
    const [changePasswordResult, setChangePasswordResult] = useState(null);

    const successChangePassword = <a style={{color: "green", fontSize: "small"}}>Successfully changed password!</a>
    const errorChangePassword = <a style={{color: "red", fontSize: "small"}}>Failed changed password!</a>

    const handleSubmitPassword = async () => {
      let credentials = {
        username: sessionStorage.getItem('username'),
        password: oldPassword,
      }
        console.log("credentials: ", credentials);
        await axios.post("http://127.0.0.1:8000/api/token/obtain/", credentials)
          .then(response => {
            const token  =  response.data;
            //sessionStorage.setItem("username", parseJwt(token.access).username);
            sessionStorage.setItem("access_token", token.access);
            sessionStorage.setItem("refresh_token", token.refresh);
            setChangePasswordResult(successChangePassword);
            //window.location.href = '/dashboard'
          })
          .catch(err => {
            setChangePasswordResult(errorChangePassword);
            console.log("err: ", err.message)
        });
    }

    return(
      <div>
          <div className="Header">
              <div className="Header-Logo">
                  <img src={logo} className="Header-Logo-Logo" alt="logo" />
              </div>
              <div className="Header-Menu-and-Title">
                  <div className="Header-Title">
                      <span className='span-title'>
                          Settings
                      </span>
                  </div>
                  <div className="Header-Menus">
                      <div className="Settings-Header-Menu-Separator">Separator</div>
                      <div className='Header-Right-Menus'>
                          <div className='Header-Right-Menu-Name'>
                              <strong className='span-right-menu'>johnsmith</strong>
                          </div>
                          <div onClick={goToDashboard} className='Header-Right-Menu'>
                              <span className='span-right-menu'>Dashboard</span>
                          </div>
                          <div className='Header-Right-Menu'>
                              <span className='span-right-menu'>Help</span>
                          </div>
                          <div onClick={logout} className='Header-Right-Menu'>
                              <span className='span-right-menu'>Logout</span>
                          </div>
                          <div>
        
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div style={{width: "fit-content", padding: "40px 0 0 100px"}}>
            <div>Settings <Button disabled={!hasChanged} variant="contained" style={{textTransform: "none"}}>Save Changes</Button></div>

            <div style={{ width: "500px", textAlign: "left", margin: "5px 5px 5px 5px"}}>
              <div style={{fontSize: "x-large", fontWeight:"600"}} className="Settings-Title-Section">Account</div>
              
                <div className="Settings-Account-Info-Fullname">
                  <div className="Settings-Full-Name-Input-Group">
                    <div>First Name</div>
                    <Input value={field.first_name}/>
                  </div>
                  <div className="Settings-Full-Name-Input-Group">
                    <div>Last Name</div>
                    <Input value={field.last_name}/>
                  </div>
                </div>

              <div className="Settings-Account-Info">
                <div>Username</div>
                <Input value={field.username}/>
              </div>
              <div className="Settings-Account-Info">
                <div>Email</div>
                <Input value={field.email}/>
              </div>

              <div className="settings-change-password-container">
                <div>Change Password</div>
                <div className="settings-change-password-icon-container" onClick={()=>{setChangePassword(!changePassword)}}>
                  <EditIcon fontSize="small"/>
                </div>
              </div>
                {changePassword ?
                <div className="Settings-Change-Password">
                  <div>
                  enter your current password:
                  </div>
                  <Input value={oldPassword} onChange={e => {setOldPassword(e.target.value)}} type="password"></Input>
                  <div>
                  enter your new password:
                  </div>
                  <Input type="password"></Input>
                  <div>
                  confirm your new password:
                  </div>
                  <Input type="password"></Input>
                  {changePasswordResult}
                  <Button onClick={handleSubmitPassword} style={{textTransform: "none"}}>Submit</Button>
                </div>
                : 
                <></>}
            </div>

            <div style={{ width: "500px", textAlign: "left", margin: "5px 5px 5px 5px"}}>
              <div>
                  <div className="Settings-Title-Section">Advanced</div>
                  <div style={{fontSize: "large", fontWeight:"300"}}>Hash function</div>
                  <div style={{fontSize: "large", fontWeight:"300"}}>Timezone</div>
                  <div className="App">
                <div className="select-wrapper">
                  <TimezoneSelect
                  style={{width: "300px"}}
                    value={selectedTimezone}
                    onChange={setSelectedTimezone}
                  />
                </div>
                <h3>Output:</h3>
                <div
                  style={{
                    backgroundColor: "#ccc",
                    padding: "20px",
                    margin: "20px auto",
                    borderRadius: "5px",
                    maxWidth: "600px",
                  }}
                >
                  <pre
                    style={{
                      margin: "0 20px",
                      fontWeight: 100,
                      fontFamily: "monospace",
                    }}
                  >
                    {JSON.stringify(selectedTimezone, null, 2)}
                  </pre>
                </div>
              </div>
              </div>
            </div>

            </div>

      </div>);

}

export default Settings;