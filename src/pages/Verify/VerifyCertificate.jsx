import { FileUploader } from "react-drag-drop-files";
import '../CertificateMenu.scss';
import { useState } from "react";
import { Button, Input } from "@mui/material";

const fileTypes = ["PDF"];
const VerifyCertificate = () => {
    const [file, setFile] = useState(null);

    const handleChange = (file) => {
      setFile(file);
    };

    return (
    <div className="Content">
        <div className="Content-Header">
            <div className="Content-Header-Title">Verify a Proof Certificate or certified PDF file</div>
        </div>
        <div>
            <FileUploader
                label={"Upload a Proof Certificate or certified PDF file"}
                multiple={false}
                handleChange={handleChange}
                name="file"
                types={fileTypes}
                style={{width: "200px"}}
            />
            <div>
                {file ? 
                <div style={{display: "flex"}}>
                <div style={{fontFamily:"monospace", backgroundColor: "lightgray"}}>{file.name}</div>
                </div>
                :
                <p>no files uploaded yet</p>
                }
            </div>
            <div>Alternatively, manually enter details from a Proof Certificate below</div>
            <div>
                <div className="Verification-Input-Form">
                    <div className="input-verification-label">Enter Proof Certificate ID:</div>
                    <Input className="wide-input-certificate"></Input>
                </div>
                <div className="Verification-Input-Form">
                    <div className="input-verification-label">Enter PDF File Hash:</div>
                    <Input className="wide-input-certificate"></Input>
                </div>
                <div className="Verification-Input-Form">
                    <Button variant="contained" style={{textTransform: 'none'}}>Verify</Button>
                </div>
            </div>
        </div>
        
    </div>
    );
}

export default VerifyCertificate;