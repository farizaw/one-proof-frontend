import { Button, Input } from '@mui/material';
import './Landing.css';

const GetStarted = () => {
    return(
        <header className="App-header">
            <p>
                <b>Get Started</b>
            </p>
            <p>
            Enter a web address which has downloadable PDFs in the box below.<br/>
            This will direct you to the web page so you can get a feel for how our website works with no obligation or cost and without logging in.
            </p>
            <div>
                <Input placeholder='Type a website address'></Input>
                <Button >Let's Go</Button>
            </div>
        </header>
        );
}
export default GetStarted