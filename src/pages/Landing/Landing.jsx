import './Landing.css';
import logo from '../../assets/oneproof.png'
import { Link } from 'react-router-dom';
import { Button, ButtonGroup } from '@mui/material';
import { useTranslation} from 'react-i18next';
import '../../i18n';

const Landing = () => {
  const { t, i18n } = useTranslation();
    return(
        <header className="App-header">
          <ButtonGroup style={{margin: "10px 0 10px 0", textTransform: "none"}} variant="contained">
            <Link to="dashboard"><Button style={{textTransform: "none"}}>Solutions</Button></Link>
            <Link to="login"><Button style={{textTransform: "none"}}>Resources</Button></Link>
            <Link to="login"><Button style={{textTransform: "none"}}>Login</Button></Link>
            <Link to="register"><Button style={{textTransform: "none"}}>Register</Button></Link>
          </ButtonGroup>
          <img src={logo} className="App-logo" alt="logo" />
          <h1>Prove Web PDFs</h1>
          <p>{t('landing.landing.title')}
          </p>
          <ButtonGroup style={{margin: "10px 0 10px 0", textTransform: "none"}} variat="contained">
            <Link to="register">
            <Button style={{textTransform: "none"}}>Sign Up and Use One Proof</Button></Link>
            <Button style={{textTransform: "none"}}>View Samples</Button>
          </ButtonGroup>
          <div>Already have an account? <Link to="/login">Login</Link></div>
        </header>
        );
}
export default Landing;