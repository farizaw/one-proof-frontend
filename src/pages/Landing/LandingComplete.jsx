
import Landing from "./Landing";
import Welcome from "./Welcome";
import About from "./About";
import GetStarted from "./GetStarted";
const LandingComplete = () => {
    return (<>
          <Landing/>
          <Welcome/>
          <About/>
          <GetStarted/>
    </>);
}

export default LandingComplete;