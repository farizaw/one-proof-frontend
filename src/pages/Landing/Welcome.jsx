import './Welcome.css';
import imagePlaceholder from '../../assets/placeholder.png';
import { Carousel } from 'react-responsive-carousel';

const Welcome = () => {
  const images = [
    imagePlaceholder,
    imagePlaceholder,
    imagePlaceholder,
    imagePlaceholder,
  ];
  
    return(
        <div className="App-header">
          <h3>Welcome to One Proof</h3>
          <p>One Proof is an easy way to show anyone that a PDF was found on a web page and readable.</p>
          <p>We generate a Proof Certificate on demand which can be used as legal evidence which is permanent and cannot be modified - not even by us.</p>
          <div className="Image-Gallery">

      <Carousel useKeyboardArrows={true}>
        {images.map((URL, index) => (
          <div className="slide">
            <img alt="sample_file" src={URL} key={index} />
          </div>
        ))}
      </Carousel>
            {/* <div className='Image-Frame'>
              <div>1. Find the web address</div>
              <img src={logox} alt = 'image1'/>
            </div>
            <div>
              <div>2. Add it to One Proof</div>
              <img alt = 'image2' src={logox}/>
            </div>
            <div>
              <div>3. Select the PDFs </div>
              <img src={logox} alt = 'image3' width={500} height={500}/>
            </div>
            <div>
              <div>4. Download your Proof Certificate </div>
              <img src={imagePlaceholder} alt="noname"/>
            </div> */}
          </div>
        </div>
        );
}
export default Welcome