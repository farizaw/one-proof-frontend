import './Landing.css';
import './About.css';
import { Button, ButtonGroup } from '@mui/material';
//import logo from '../../assets/oneproof.png'

const About = () => {
    return(
        <header className="App-header">
          <div className='About-Why-Examples'>
            <div className='About-Why'>
                <b>Why would you want to use our service?</b>
                <ul style={{fontSize: "20px"}}>
                    <li>How about you had an important notice on your website and someone says you never put it there - how do you prove it was published?</li>
                    <li>Say you entered into an agreement using someone's website terms and conditions found in a PDF - how do you prove the version you agreed to?</li>
                </ul>
            </div>
            <div className='About-Examples'>
                <b>Example web PDFs we certify on demand:</b>
                <ul style={{fontSize: "20px"}}>
                    <li>Public Notices</li>
                    <li>Compliance Reports</li>
                    <li>Terms and Conditions</li>
                    <li>Advertisements</li>
                    <li>Annual Reports</li>
                    <li>Corporate Governance Statements</li>
                    <li>Assessment Reports</li>
                    <li>Management Plans</li>
                    <li>Safety Data Sheets</li>
                    <li>Environment Reports</li>
                    <li>Journal Articles</li>
                    <li>...etc etc…</li>
                </ul>
            </div>
          </div>
          <div className='About-Plus'>
            <p><i>Plus</i> our website allows you to <u>audit</u> your Proof Certificate to demonstrate it is original and genuine.</p>
            </div>
            <ButtonGroup variant="contained">
                <Button style={{textTransform: "none"}}>Learn More</Button>
                <Button style={{textTransform: "none"}}>Case Studies</Button>
                <Button style={{textTransform: "none"}}>FAQs</Button>
            </ButtonGroup>
            <div>Get your proof certificate. Have piece of mind</div>
        </header>
        );
}

export default About;